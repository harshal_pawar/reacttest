import logo from './logo.svg';
import './App.scss';
import Tooltip from './components/tooltip/Tooltip';
function App() {
  return (
    <div className='container'>
      <p>Here is a <Tooltip message={'Tooltip'} position={'right'}>tooltip</Tooltip> on right.</p>

      <p>
      The steps to add Sass to Create React App are:<Tooltip message={'Tooltip'} position={'top'}>tooltip</Tooltip>  Install node-sass: yarn add node-sass. Convert your . css to . scss. Import your . scss files in your React components like App. js.
      </p>
    </div>
      
  );
}
export default App;
